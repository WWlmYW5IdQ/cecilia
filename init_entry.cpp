#include "cecilia.h"

void init_entry(char c) {
  int n;
  std::vector<std::filesystem::path> vec;
  std::regex e(std::string("(.*)") + std::string(1, c) + std::string("(\\d+).tex"));
  get_file_by_regex(&vec, e);
  n = vec.size();
  std::string filename = std::string(1, c) + std::to_string(n) + ".tex";
  std::filesystem::copy("template.tex", filename);
}
