#include "cecilia.h"

int main(int argc, char** argv) {
  std::vector<std::string> arg;
  parse_arg(&arg, argc, argv);
  if (arg.size() == 3) {
    if (arg[1] == "new") {
      if (arg[2] == "memo") {
        init_entry('m');
      } else if (arg[2] == "notes") {
        init_entry('n');
      }
    }
  }
  return 0;
}
