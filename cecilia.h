#ifndef CECILIA_H
#define CECILIA_H

#include <filesystem>
#include <fstream>
#include <iostream>
#include <regex>
#include <string>
#include <vector>

void get_file_by_regex(std::vector<std::filesystem::path>*, std::regex);
void init_entry(char);
void parse_arg(std::vector<std::string>*, int, char**);

#include "get_file_by_regex.cpp"
#include "init_entry.cpp"
#include "parse_arg.cpp"

#endif
