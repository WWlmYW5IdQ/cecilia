#include "cecilia.h"

void get_file_by_regex(std::vector<std::filesystem::path>* vec, std::regex e) {
  std::string str;
  std::filesystem::path dir = std::filesystem::current_path();
  for (const auto& i : std::filesystem::directory_iterator(dir)) {
    str = i.path();
    if (std::regex_match(str, e)) {
      vec->push_back(i);
    }
  }
}
